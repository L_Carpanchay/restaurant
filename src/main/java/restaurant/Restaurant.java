import java.lang.reflect.Array;
import java.util.ArrayList;


public class Restaurant{
    private String nombreDelRestaurant;
    Integer i;

    public void setNombreDelRestaurant(String vnombre){
        this.nombreDelRestaurant = vnombre;
    }
    public String setNombreDelRestaurant(){
        return nombreDelRestaurant;
    }

    ArrayList<Cliente> listaDeClientes = new ArrayList<Cliente>();
    ArrayList<OrdenDeServicio> listaDeOrdenDeServicios = new ArrayList<OrdenDeServicio>();
    ArrayList<Cuenta> listaDeCuentas = new ArrayList<Cuenta>();
    ArrayList<TarjetaDeCredito> tarjetasDeCredito = new ArrayList<TarjetaDeCredito>();

    public void AgregarCliente(String nombre, String apellido){
        Cliente cliente = new Cliente(nombre, apellido);
        listaDeClientes.add(cliente);
    }

    public void iniciarOrden(Integer cantComensales, Integer cantPlatos, Integer cantBebidas, Integer numMesa, String horaIngreso, String horaEgreso, String nombreMozo){
        OrdenDeServicio orden = new OrdenDeServicio(cantComensales, cantPlatos, cantBebidas, numMesa, horaIngreso, horaEgreso, nombreMozo);
        listaDeOrdenDeServicios.add(orden);
    }

    public void cerrarOrden(Double costo, Integer opcion){
        Cuenta pagar = new Cuenta();
        pagar.setCosto(costo);
        if(opcion==1){
            pagar.setOpcionDeTarjeta(1);
            pagar.CalculaRecargo();
            pagar.CalculaCostoTotal();
            listaDeCuentas.add(pagar);
        }
        else{
            pagar.setOpcionDeTarjeta(0);
            pagar.CalculaCostoTotal();
            listaDeCuentas.add(pagar);
        }
        
    }

    public void agregarTarjeta(String titular, String numTarjeta, String codigoTargeta){
        TarjetaDeCredito tarjeta = new TarjetaDeCredito(numTarjeta, codigoTargeta, titular);
        tarjetasDeCredito.add(tarjeta);
    }

    public void mostrarClientes(){
        for(i=0; i<listaDeClientes.size(); i++){
            listaDeClientes.get(i).mostarPorPantalla();
        }

    }
    public void mostrarInformacionDePedidos(){
        for(i=0; i<listaDeClientes.size(); i++){
            listaDeClientes.get(i).mostarPorPantalla();
            listaDeOrdenDeServicios.get(i).mostarPorPantalla();
            System.out.println(listaDeCuentas.get(i).getCostoTotal());
            if(listaDeCuentas.get(i).getOpcionDeTarjeta()==1){
                System.out.println("El pago fue con tarjeta de credito");
            }
            else{
                System.out.println("El pago fue con efectivo");
            }
        }
    }
}