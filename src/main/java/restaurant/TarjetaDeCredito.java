public class TarjetaDeCredito{
    private String titular;
    private String numeroDeLaTarjeta;
    private String codigoSeguridad;

    public TarjetaDeCredito(String vtitular, String numTarjeta, String codigoTargeta){
        this.titular = vtitular;
        this.numeroDeLaTarjeta = numTarjeta;
        this.codigoSeguridad = codigoTargeta;
    }

    public void setTitular(String vtitular){
        this.titular = vtitular;
    }

    public String getTitular(){
        return titular;
    }

    public void setNumeroDeLaTarjeta(String vnumeroDeLaTarjeta){
        this.numeroDeLaTarjeta = vnumeroDeLaTarjeta;
    }
    public String getNumeroDeLaTarjeta(){
        return numeroDeLaTarjeta;
    }

    public void setCodigoSeguridad(String vcodigoSeguridad){
        this.codigoSeguridad = vcodigoSeguridad;
    }
    public String getCodigoSeguridad(){
        return codigoSeguridad;
    }

    public void mostrarPorPantalla(){
        System.out.println("Titular de la Tarjeta de Credito: "+titular+" con tarjeta terminada en: "+numeroDeLaTarjeta.charAt(8)+numeroDeLaTarjeta.charAt(9)+numeroDeLaTarjeta.charAt(10)+numeroDeLaTarjeta.charAt(11));
    }
}