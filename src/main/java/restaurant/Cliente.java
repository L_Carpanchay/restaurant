public class Cliente{
    private String nombreCliente;
    private String apellidoCliente;

    public Cliente(String vnombre, String vapellido){
        this.apellidoCliente = vapellido;
        this.nombreCliente = vnombre;
    }

    public void setNombreCliente(String vnombreCliente){
        this.nombreCliente = vnombreCliente;
    }
    public String getNombreCliente(){
        return nombreCliente;
    }
    
    public void setApellidoCliente(String vApellidoCliente){
        this.apellidoCliente = vApellidoCliente;
    }
    public String getApellidoCliente(){
        return apellidoCliente;
    }

    public void mostarPorPantalla(){
        System.out.println("Nombre: "+nombreCliente);
        System.out.println("Apellido: "+apellidoCliente);
    }
}