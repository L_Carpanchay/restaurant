
public class OrdenDeServicio{
    private Integer cantidadDeComensales;
    private Integer cantidadDePlatos;
    private Integer cantidadDeBebidas;
    private Integer numeroDeMesa;
    private String horaDeEntrada;
    private String horaDeSalida;
    private String nombreMozo;

    public OrdenDeServicio(Integer cantComensales, Integer cantPlatos, Integer cantBebidas, Integer numMesa, String horaIngreso, String horaEgreso, String nombreMozo){
        this.cantidadDeComensales = cantComensales;
        this.cantidadDePlatos = cantPlatos;
        this.cantidadDeBebidas = cantBebidas;
        this.numeroDeMesa = numMesa;
        this.horaDeEntrada = horaIngreso;
        this.horaDeSalida = horaEgreso;
        this.nombreMozo = nombreMozo;
    }

    public void setCantidadDeComensales(Integer vComensales){
        this.cantidadDeComensales = vComensales;
    }
    public  Integer getCantidadDeComensales(){
        return cantidadDeComensales;
    }

    public void setCantidadDePlatos(Integer vPlatos){
        this.cantidadDePlatos = vPlatos;
    }
    public Integer getCantidadDePlatos(){
        return cantidadDePlatos;
    }

    public void setCantidadDeBebidas(Integer vBebidas){
        this.cantidadDeBebidas = vBebidas;
    }
    public Integer getCantidadDeBebidas(){
        return cantidadDeBebidas;
    }

    public void setNumeroDeMesa(Integer vMesa){
        this.numeroDeMesa = vMesa;
    }
    public Integer getNumeroDeMesa(){
        return numeroDeMesa;
    }

    public void setHoraDeEntrada(String ventrada){
        this.horaDeEntrada = ventrada;
    }
    public  String getHoraDeEntrada(){
        return horaDeEntrada;
    }

    public void setHoraDeSalida(String vsalida){
        this.horaDeSalida = vsalida;
    }
    public String getHoraDeSalida(){
        return horaDeSalida;
    }

    public void setNombreMozo(String vMozo){
        this.nombreMozo = vMozo;
    }
    public String getNombreMozo(){
        return nombreMozo;
    }

    public void mostarPorPantalla(){
        System.out.println("Numero de Comensales: "+cantidadDeComensales);
        System.out.println("Cantidad de Platos: "+cantidadDePlatos);
        System.out.println("Cantidad de Bebidas: "+cantidadDeBebidas);
        System.out.println("Numero de Mesa: "+numeroDeMesa);
        System.out.println("Nombre del Mozo: "+nombreMozo);
        System.out.println("Hora de Entrada: "+horaDeEntrada);
        System.out.println("Hora de Saldia: "+horaDeSalida);
    }

}