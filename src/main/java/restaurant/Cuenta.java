public class Cuenta{
    private Double costo;
    private Double recargo;
    private Double costoTotal;
    private Integer porcentajeDeRecargo = 17;
    private Integer opcionDeTarjeta; //si es 1 es porque se usara una tarjeta de credito, si es 0 no.
    
    public void setCosto(Double vcosto){
        this.costo = vcosto;
    }
    public Double getCosto(){
        return costo;
    }

    public void setOpcionDeTarjeta(Integer vopcion){
        this.opcionDeTarjeta = vopcion;
    }
    public Integer getOpcionDeTarjeta(){
        return opcionDeTarjeta;
    }

    public void CalculaRecargo(){
        recargo = costo * (porcentajeDeRecargo / 100);
    }
    public Double getRecargo(){
        return recargo;
    }

    public void CalculaCostoTotal(){
        if(opcionDeTarjeta==1){
            this.costoTotal = costo + recargo;
        }
        else{
            this.costoTotal = costo;
        }
    }
    public Double getCostoTotal(){
        return costoTotal;
    }
}